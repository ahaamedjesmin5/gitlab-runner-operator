import image
import subprocess
import config


def image_with_digest(name, arch=""):
    docker_image = image.get(name, arch)

    if not config.certified():
        return docker_image

    subprocess.run(["docker", "pull", docker_image])

    cmd = subprocess.run(["docker", "image", "inspect", "--format",
                          "{{ index .RepoDigests 0 }}", docker_image], stdout=subprocess.PIPE)
    cmd.check_returncode()
    digest = cmd.stdout.decode("utf-8").strip()

    return digest


def push(tag):
    print(f"Pushing Docker image: {tag}")

    cmd = subprocess.run(
        ["docker", "push", tag], stderr=subprocess.PIPE)
    cmd.check_returncode()
    stderr = cmd.stderr.decode('utf-8').strip()

    if "The image tag you are pushing already exists" in stderr:
        print(f"Image {tag} already exists")
    else:
        if len(stderr) > 0:
            raise Exception(stderr)


def manifest_create(manifest, tags):
    print(f"Creating Docker manifest {manifest} for {tags}")

    for tag in tags:
        subprocess.run(["docker", "pull", tag]).check_returncode()

    subprocess.run(
        ["docker", "manifest", "rm", manifest], stderr=subprocess.DEVNULL)

    subprocess.run(["docker", "manifest", "create",
                   manifest, *tags]).check_returncode()


def manifest_push(manifest):
    print(f"Pushing Docker manifest {manifest}")

    subprocess.run(["docker", "manifest", "push", manifest]).check_returncode()
